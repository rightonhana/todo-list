import React from "react";

export const ToDoItem = ({ value, done, onDone, onDelete }) => (
    <li>
        <span>{value}</span>
        <span>{done? ' - done' : ' - not done'}</span>
        <input
            type="checkbox"
            checked={done}
            onChange={event => { onDone(); }}
        />
        <button
            onClick={event => { event.preventDefault(); onDelete(); }}
        >&times;</button>
    </li>
);

export default ToDoItem;