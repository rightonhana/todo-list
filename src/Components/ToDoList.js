import React, { Component } from "react";
import { ToDoAdd } from "./ToDoAdd";
import { ToDoItems } from "./ToDoItems";

export class ToDoList extends Component {
    constructor (props) {
      super(props);
      this.state = {
        items: []
      };
      this.onDone = this.onDone.bind(this);
    }

    onAdd = (value) => {
        this.setState({
            items: [ ...this.state.items, {
                value: value,
                done: false
            } ]
        });
    }

    onDone(index) {
        this.setState((state) => ({
            items: [
                ...state.items.slice(0, index),
                {
                    ...state.items[index],
                    done: !state.items[index].done
                },
                ...state.items.slice(index + 1)
            ]
        }));
    }

    onDelete = (index) => {
        this.setState({
            items: [
                ...this.state.items.slice(0, index),
                ...this.state.items.slice(index+1)
            ]
        });
    }

    render() {
        const { items } = this.state;
        const { onAdd, onDone, onDelete } = this;

        return (
            <div>
                <ToDoAdd onAdd={onAdd}/>
                <ToDoItems items={items} onDone={onDone} onDelete={onDelete}/>
            </div>
        );
    }
}

export default ToDoList;