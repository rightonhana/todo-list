import React from "react";
import { ToDoItem } from "./ToDoItem.js";

export const ToDoItems = ({ items, onDone, onDelete }) => (
    <ul>
        {items.map((item, index) => (
            <ToDoItem
                key={index}
                {...item}
                onDone={() => onDone(index) }
                onDelete={() => onDelete(index) }
            />
        ))}
    </ul>
);

export default ToDoItems;